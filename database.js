// database==> database is a container where data operation are performed
// CRUD ==> create, read update delete

// to perfrom CRUD we use Database management system
// two type database manangement system
// 1 relational (RDBMS)
// 2 distribute (DDBMS)

// 1. RDBMS
// a. Table based design
// eg LMS ==> books,users,notification
// b. tuple/row 
// c. schema based design
// d. non scalable
// e. relation between table exists
// f. types ==> msSql,mysql,
// g. SQL stuructured query language

// 2.DDBMS
// a. Collection based design
// eg LMS==> books, users,reviews
// b. document  ==> valid json
// document based database
// c. schema less design
// d. highly scalable
// e. relation doesnot exists
//  books:{
//      name:'rich dad poor dad',
//      author:{
//          name:'ram',
//          email:'hi'
//      }
//  }
// f. types ==> mongodb, couchdb, dynamodb,redis
// NO SQL not only sql

// mongo shell command
// to access mongoshell
// mongod==> intilize driver at start server at port 27017
// mongo==> will give the arrow head interface to accept shell command >

// command 
// show dbs  ==> list all the available database
// use <db_name> if(db_name) exists selet it else create a new db and select it
// db ==> check selected db
// show collections==> list all the available collections 

//CRUD
// C create
// db.<collection_name>.insert({valid json});

// READ 
// db.<collection_name>.find({query builder})
// db.<collection_name>.find({query builder}).count() // number of items
// db.<collection_name>.find({query builder}).pretty() // formatted view

// U update
// db.<collection_name>.update({},{},{});
// 1st object==> query builder
// 2nd object must have $set as an key with value as object to be updated
// 3rd object is optional ==> options multi ,upsert

// D delete
// db.<collection_name>.remove({})
// NOTE dont leave it empty

// drop collection
// db.<col_name>.drop()
// drop database 
// db.dropDatabase();

// ODM ==> object document modelling
// mongoose is used for nodejs with mongodb
// advantages of using mongoose
// 1. Schema based solution
// 2.indexing is lot more easier
// 3. methods for operation
// 4. datatypes ==> 
// 5.middleware ==> 


//****BACKUP & RESTORE */
// bson data (machine readable) / json &csv (human readable)
// 1 machine readable
// backup 
// command
// mongodump ==> it will create a backup of all the available database in default dump
// mongodump --db <db_name> ==> it will backup selected db only inside default dump folder
// mongodump --db <db_name> --out <path_to_output_directory>

// restore 
// command
// mongorestore ===> it tries to restore all the database from default dump folder
// mongorestore --drop  ==> drop the duplicate document from database and override with backup data
// mongorestore path_to_backup_folder
// mongodump and mongorestore always cames in pair


// mongoexport and mongoimport for human readable(JSON and CSV)
// backup
// mongoexport --db <db_name> --collection<collection_name> --out path_to_destinatin_with.json format
// mongoexport -d <db_name> -c <collection_name> -o path_to_destinatin_with.json format

// restore
// mongoimport --db<db_name> --collection<col_name> source_file

// csv format
// backup
// mongoexport --db <db_name> --collection<col_name> --type=csv --fields 'comma seperated value' --out destination folder with .csv extenstion
// mongoexport --db <db_name> --collection<col_name> --type=csv --query="{key:'value'}" --fields 'comma seperated value' --out destination folder with .csv extenstion

// restore 
// mongoimport --db <db_name> --collection<col_name> --type=csv path_to_soruce_with.csv --headerline

