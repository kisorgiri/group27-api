const http = require('http');

const write = require('./write');


const server = http.createServer(function (request, response) {
    console.log('client connected to server');
    console.log('request url is >>', request.url);
    console.log('request method is >>', request.method);
    // request or 1st argument is always http request object
    // response or 2nd argument is alwasy http response object
    // for each client request this callback is executed
    // request response cycle must be completed
    // regardless any http method and url this callback is executed

    if (request.url === '/write') {
        // fs 
        write('a.txt', 'b')
            .then(function (data) {
                response.end('success');
            })
            .catch(function (err) {
                response.end('failure')
            })
    } else if (request.url === 'read') {
        // fs
    } else {

        response.end('Hello from Node Server')
    }
});

server.listen(9090, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port 9090 inside');
        console.log('press CTRL + C to exit');
    }
});
