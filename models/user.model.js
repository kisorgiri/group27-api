const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    // actual modelling
    name: String,
    phoneNumber: {
        type: Number,
        minlength: 10
    },
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    dob: {
        type: Date
    },
    address: {
        temp_address: [String],
        permanent_address: String
    },
    status: {
        type: String,
        default: 'inactive'
    },
    role: {
        type: Number, //1 admin, 2 general user, 3 visitor
        default: 2
    },
    image: {
        type: String
    }
}, {
    timestamps: true
})

const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;
