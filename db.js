const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');

mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, function (err, done) {
    if (err) {
        console.log('error connecting to db >>', err);
    } else {
        console.log('db connection successfull');
    }
})