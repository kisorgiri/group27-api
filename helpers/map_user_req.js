module.exports = function (user, userDetails) {
    if (userDetails.role)
        user.role = userDetails.role;
    if (userDetails.status)
        user.status = userDetails.status;
    if (userDetails.phoneNumber)
        user.phoneNumber = userDetails.phoneNumber;
    if (userDetails.full_name)
        user.name = userDetails.full_name;
    if (userDetails.email)
        user.email = userDetails.email;
    if (userDetails.username)
        user.username = userDetails.username
    if (userDetails.password)
        user.password = userDetails.password
    if (userDetails.gender)
        user.gender = userDetails.gender;
    if (userDetails.date_of_birth)
        user.dob = userDetails.date_of_birth;
    if (userDetails.image)
        user.image = userDetails.image;
    if (!user.address) {
        user.address = {};
    }
    if (userDetails.temp_address)
        user.address.temp_address = userDetails.temp_address.split(',')
    if (userDetails.permanent_address)
        user.address.permanent_address = userDetails.permanent_address;

    return user;
} 