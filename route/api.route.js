const router = require('express').Router();

const authRouter = require('./../controllers/auth.controller');
const userRouter = require('./../controllers/user.controller');
const productRouter = require('./../modules/products/product.route');


const authenticate = require('./../middlewares/authenticate');

router.use('/auth', authRouter);
router.use('/user', authenticate, userRouter);
router.use('/product', productRouter);


module.exports = router;