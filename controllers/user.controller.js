const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');

router.route('/')
    .get(function (req, res, next) {
        // req will have user
        var condition = {};
        UserModel
            .find(condition, { password: 0 })
            .sort({
                _id: -1 //descending order
            })
            // .limit(2)
            // .skip(2)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            })
    })
    .post(function (req, res, next) {

    });

router.route('/search')
    .get(function (req, res, next) {
        res.json({
            msg: 'from user search'
        })
    })

router.route('/:id')
    .get(function (req, res, next) {
        // req.will have user property
        // UserModel.findOne({
        //     _id: req.params.id
        // })
        //     .then(function (user) {
        //         res.json(user);
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     })
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            res.json(user);
        })
    })
    .put(function (req, res, next) {
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            // user is also mongoose object
            const updatedMappedUser = MapUser(user, req.body);

            updatedMappedUser.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })

        })
    })
    .delete(function (req, res, next) {
        if (req.user.role !== 1) {
            return next({
                msg: "You dont have permission",
                status: 403
            })
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User not found",
                    status: 404
                })
            }
            user.remove(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
    });



module.exports = router;