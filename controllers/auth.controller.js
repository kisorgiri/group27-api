const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/map_user_req');
const uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');

function getToken(data) {
    let token = jwt.sign({
        _id: data._id
    }, config.jwtSecret);
    return token;
}

router.get('/user', function (req, res, next) {
    require('fs').readFile('sdklf.sd.kfj', function (err, done) {
        if (err) {
            return req.myEvent.emit('error', err, res);
        }
    })
})

router.get('/', function (req, res, next) {
    console.log('directory path >>', __dirname);
    console.log('root path >>', process.cwd());

    next();
}, function (req, res, next) {
    res.json({
        msg: "hi from empty auth route nested middleware"
    })
})

router.get('/login', function (req, res, next) {
    res.render('login.pug');
})

router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            username: req.body.username
        })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // password verification
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {
                    // token genration
                    var token = getToken(user);
                    res.json({
                        user: user,
                        token: token
                    })

                } else {
                    return next({
                        msg: 'Invalid Password',
                        status: 400
                    })
                }

            } else {
                return next({
                    msg: 'Invalid Username',
                    status: 400
                })
            }
        })
})

router.get('/register', function (req, res, next) {
    res.json({
        msg: "hi from register route"
    })
})

router.post('/register', uploader.single('image'), function (req, res, next) {
    // db stuff
    console.log('req.body>>', req.body);
    console.log('req.file >>', req.file);
    if (req.fileTypeError) {
        return next({
            msg: "Invalid File Format",
            status: 400
        })
    }
    var newUser = new UserModel({});
    // newuser mongoose object
    if (req.file) {
        req.body.image = req.file.filename;
    }

    MapUser(newUser, req.body)

    // newUser.save(function (err, done) {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.json(done);
    // })
    newUser.password = passwordHash.generate(req.body.password);
    newUser
        .save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

})

module.exports = router;