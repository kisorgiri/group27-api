const path = require('path');
const multer = require('multer');
// const upload = multer({
//     dest: path.join(process.cwd(), 'uploads')
// })
const myStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    },
    destination: function (req, file, cb) {
        cb(null, path.join(process.cwd(), 'uploads/images/'))
    }
})

function filter(req, file, cb) {
    var mimetype = file.mimetype.split('/')[0];
    if (mimetype === 'image') {
        cb(null, true);
    } else {
        req.fileTypeError = true;
        cb(null, false)
    }
}
const uploader = multer({
    storage: myStorage,
    fileFilter: filter
})

module.exports = uploader;