module.exports = function (req, res, next) {
    if (req.query.role === 'admin') {
        next();
    } else {
        next({
            msg: 'You are not authorized',
            status: 403
        })
    }
}