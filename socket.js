const socket = require('socket.io');
var users = [];
module.exports = function (app) {
    console.log('here at this file')
    const io = socket(app.listen(8081, function (err) {
        if (err) {
            console.log('socket server listening failed', err)
        } else {
            console.log('socket server listening at port 8081')
        }
    }),
        {
            cors: {
                origin: "http://localhost:3000",
                methods: ["GET", "POST", "PUT", "DELETE"]
            }
        }
    );
    io.on('connection', function (client) {
        console.log('client connected to socket server')
        var id = client.id;
        client.on('new-msg', function (data) {
            client.emit('reply-msg-own', data);
            client.broadcast.to(data.receiverId).emit('reply-msg', data);
        })

        client.on('new-user', function (username) {
            users.push({
                id: id,
                name: username
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })

        client.on('disconnect', function () {
            users.forEach(function (user, i) {
                if (user.id === id) {
                    users.splice(i, 1);
                }
            });
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
    })

}