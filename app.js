const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
// app is entire express framework


app.set('port', 8080);


// my own error handler middleware
const events = require('events');
const event = new events.EventEmitter();

app.use(function (req, res, next) {
    req.myEvent = event;
    next();
})

event.on('error', function (error, res) {
    console.log('error in error handler', error);
    res.status(400).json(error)
})

// connect to db
require('./db');


// view engine setup
const pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(__dirname, 'views'));
// import routing level middleware
const apiRoute = require('./route/api.route');


// load third party middleware
app.use(morgan('dev'));
app.use(cors());

// socket stuff
require('./socket')(app)


// inbuilt middleware
app.use(express.static(path.join(__dirname, 'uploads'))); // internal request serve 
app.use('/file', express.static(path.join(__dirname, 'uploads'))) // external request serve
console.log('directory path >>', __dirname);

// parse incoming data
app.use(express.urlencoded({
    extended: true
}))
// this will parse incoming data and add the data in body property of request
app.use(express.json());

// load routing level middleware
app.use('/api', apiRoute);

// middleware will work as 404 error handler
app.use(function (req, res, next) {
    next({
        msg: 'Not Found',
        status: 404
    })
})

// error handling middleware function
// 1st is for error,
// 2nd for req , 3rd res and 4th next
// to execute error handling middleware we must call next with argument
// the argument passed in next is value receive in 1st placeholder of error handling middleware
app.use(function (err, req, res, next) {
    console.log('here at error handling middleware');
    res.status(err.status || 400);
    res.json({
        text: 'from error handling middleware',
        msg: err.msg || err,
        status: err.status || 400
    })
})


app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('error listening')
    } else {
        console.log('server listening at port' + app.get('port'));
    }
});

// middleware ==> middleware is a function which has access to 
// http request object
// http response object
// next middleware function refrence

// middleware always came into action in between request response cycle
// middleware is a powerful function which can modify request and response object

// the order of middleware matters

//syntax
// function(req,res,next){
//     // req or 1st argument is http request object
//     // res or 2nd argument is http response object
//     // next is next middleware function reference
// }

// configuration block
// app.use() it is a configuration block for middleware

// there are 5 types of middleware
// 1 application level middleware
// 2 routing level middleware
// 3 third party middleware
// 4 inbuilt middleware
// 5 error handling middleware

// 1 application level middleware
// a function having a scope of req res and next


// routing level middleware