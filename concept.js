// nodejs==> server side run time environment
// after installation of nodejs we will have two tool NPM NPX
// NPM is javascript package management tool
// NPM is used to mange overal JS project

// command
// node -v ,npm-v
// npm init ==> initialize a JS project by creating a pacakge.json file
// package.json file is a overall project introductory file
// npmjs.com==> global repository to hold multiple JS packages
// npm install <pacakge_name> pacakge_name should be from npmjs.com
// packge-lock.json it maintais overall verson of the dependant packages
// node_modules==> it is folders holding all the pacakages from npmjs.com

// npm install ==> install all the dependant packages registered on dependancy section of package.json file

// uninstall
// npm uninstall <package_name>

// // file file communication // module-module communication
//in js a single file or a complete project is called as a module

// es5
// one file must export
// and another should import

// export syntax
// module.exports = J PANI lesson1(JS IS LOOSELY TYPED PROGRAMMING LANGUAGE)

// import syntax
// const abc = require('path To file')

//when importing nodejs own module and node_module's module  we should not give path
// const a = require('ngx-toastr');
// const b = require('events');
// const concet = require('./concept');

const fs = require('fs');
// const write = require('./write');
// write('a.txt','b')
//     .then(function(data){
//         console.log('success ',data);
//     })
//     .catch(function(err){
//         console.log('err >>',err);
//     })
// fs.writeFile('./sdkljf/test.txt','HI and welcome to node JS',function(err,done){
//     if(err){
//         console.log('erro is >>',err);
//     }else{
//         console.log('success in weite >>',done);
//     }
// })

// fs.readFile('./test.txt',function(err,done){
//     if(err){
//         console.log('err is .>',err);
//     }else{
//         console.log('done is >>',done.toString());
//     }
// })

fs.rename('./abc.txt', './xyz.txt', function (err, done) {
    if (err) {
        console.log('rename failed');
    } else {
        console.log('rename sucess');
    }
})

fs.unlink('./abc.txt', function (err, done) {
    if (err) {
        console.log('remove failed');
    } else {
        console.log('remove sucess');
    }
})

// preapre a seperate file for write and read also make one main file
// write and read must be a function and you should handle the result of that function
// export write and read from those file
// import write and read on main file and execute it


// REST API
// REST and API
// API ==> Application programming interface
// protocol will have set of rules to communicate
// http protocol ==> method and url
// combination of method and url is called API
// GET/users ==> API 


// REST ==> representational state transfer
// 1. Stateless ==> ok
// 2. correct use of http verb GET=>fetch, POST=>insert, PUT/PATCH=>update,DELETE=>remove
// we have followed Correct way of communication using http verb
// 3. data format must be either can be xml or json 
// 4.caching only for get request
