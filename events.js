const events = require('events');

const myEvent = new events.EventEmitter();
const myEvent2 = new events.EventEmitter();
myEvent.on('kishor', function (data) {
    console.log('kishor event fired', data);
})

setTimeout(function () {
    myEvent.emit('kishor', 'test something')
}, 3000)

// on ==> listen
// emit ==> trigger