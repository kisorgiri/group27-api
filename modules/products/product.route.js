const router = require('express').Router();
const ProductCtrl = require('./product.controller');
const authenticate = require('./../../middlewares/authenticate');
const uploader = require('./../../middlewares/uploader');


router.route('/')
    .get(authenticate, ProductCtrl.findAll)
    .post(authenticate, uploader.single('image'), ProductCtrl.insert);

router.route('/search')
    .get(ProductCtrl.search)
    .post(ProductCtrl.search)
router.route('/delete')
    .get(authenticate, ProductCtrl.remove)

router.route('/:id')
    .get(authenticate, ProductCtrl.findById)
    .put(authenticate, uploader.single('image'), ProductCtrl.update)
    .delete(authenticate, ProductCtrl.remove);

module.exports = router;

