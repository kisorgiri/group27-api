const ProductQuery = require('./product.query');


function findAll(req, res, next) {

    var condition = {};
    ProductQuery.find(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err)
        })
}

function findById(req, res, next) {
    var condition = { _id: req.params.id };
    ProductQuery.find(condition)
        .then(function (data) {
            res.json(data[0]);
        })
        .catch(function (err) {
            next(err);
        })
}

function search(req, res, next) {
    var condition = {};
    ProductQuery.find(condition)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

function insert(req, res, next) {
    console.log('req.body>>>', req.body);
    console.log('req.file >>', req.file);
    var data = req.body;
    data.vendor = req.user._id;
    if (req.file) {
        data.images = req.file.filename;
    }
    // images todo
    ProductQuery
        .insert(data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function update(req, res, next) {
    // TODO remove existing image if new image is updated
    var data = req.body;
    data.user = req.user._id;
    if (req.file) {
        data.images = req.file.filename
    }
    data.vendor = req.user._id;
    ProductQuery
        .update(req.params.id, data)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function remove(req, res, next) {
    ProductQuery
        .remove(req.params.id)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
}


module.exports = {
    findAll,
    findById,
    search,
    insert,
    update,
    remove
}