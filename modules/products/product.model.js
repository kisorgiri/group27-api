const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const reviewSchema = new Schema({
    point: Number,
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: {
        type: String
    },
    description: String,
    category: {
        type: String,
        required: true
    },
    brand: String,
    modelNo: String,
    price: Number,
    quantity: Number,
    images: [String],
    reviews: [reviewSchema],
    offers: [String],
    tags: [String],
    size: String,
    color: String,
    manuDate: Date,
    expiryDate: Date,
    vendor: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'qunatity', 'value']
        },
        discountValue: String
    },
    status: {
        type: String,
        enum: ['out of stock', 'available', 'booked'],
        default: 'available'
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('product', ProductSchema);