const ProductModel = require('./product.model');

function map_product_req(product, productDetails) {
    console.log('productdetials .>', productDetails);
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate;
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate;
    if (productDetails.offers)
        product.offers = typeof (productDetails.offers) === 'string' ? productDetails.offers.split(',') : productDetails.offers;
    if (productDetails.tags)
        product.tags = typeof (productDetails.tags) === 'string' ? productDetails.tags.split(',') : productDetails.tags;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.quantity)
        product.quantity = productDetails.quantity;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.vendor)
        product.vendor = productDetails.vendor;
    if (productDetails.size)
        product.size = productDetails.size;
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.name)
        product.name = productDetails.name;
    if (!product.discount)
        product.discount = {};
    if (productDetails.discountedItem)
        product.discount.discountedItem = productDetails.discountedItem;
    if (productDetails.discountType)
        product.discount.discountType = productDetails.discountType;
    if (productDetails.discountValue)
        product.discount.discountValue = productDetails.discountValue;
    if (productDetails.images)
        product.images = productDetails.images;
    if (productDetails.reviewPoint && productDetails.reviewMessage) {
        var review = {
            point: productDetails.reviewPoint,
            message: productDetails.reviewMessage,
            user: productDetails.user
        }
        product.reviews.push(review);
    }

}

function find(condition) {
    return ProductModel.find(condition)
        .populate('vendor', {
            username: 1,
            email: 1
        })
}


function insert(data) {
    var newProduct = new ProductModel({});

    map_product_req(newProduct, data);
    return newProduct.save()

}

function remove(id) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id, function (err, product) {
            if (err) {
                return reject(err);
            }
            if (!product) {
                return reject({
                    msg: 'Product not found',
                    status: 404
                })
            }
            product.remove(function (err, done) {
                if (err) {
                    reject(err);
                } else {
                    resolve(done);
                }
            })
        })
    })

}

function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel
            .findById(id)
            .exec(function (err, product) {
                if (err) {
                    return reject(err);
                }
                if (!product) {
                    return reject({
                        msg: 'Product Not Found',
                        status: 404
                    })
                }
                // product is mongoose object
                map_product_req(product, data);
                product.save(function (err, updated) {
                    if (err) {
                        return reject(err);
                    }
                    resolve(updated)
                })
            })
    })

}


module.exports = {
    find,
    insert,
    remove,
    update
}